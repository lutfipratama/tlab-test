#!/bin/bash

echo -n "masukkan bilangan: "
read bilangan

if [ $bilangan -eq 2 ]
then
    echo "$bilangan ini adalah bilangan prima"
    exit 0
fi

if [ $bilangan -lt 2 ] || [ $((bilangan%2)) -eq 0 ]
then
    echo "$bilangan ini bukan bilangan prima"
    exit 1
fi

for ((i=3; i*i<=bilangan; i+=2))
do
    if [ $((bilangan%i)) -eq 0 ]
    then
        echo "$bilangan ini bukan bilangan prima"
        exit 1
    fi
done

echo "$bilangan ini adalah bilangan prima"
exit 0
